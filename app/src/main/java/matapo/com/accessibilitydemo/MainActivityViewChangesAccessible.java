package matapo.com.accessibilitydemo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivityViewChangesAccessible extends AppCompatActivity {

    private EditText mNameEditText;
    private EditText mEmailEditText;
    private List<Person> mPersons;
    private CoordinatorLayout mCoordinatorLayout;
    private RecyclerView mPersonsRecyclerView;
    private PersonsRecyclerViewAdapter mPersonsRecyclerViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_view_changes_accessible);

        mPersons = new ArrayList<>();
        mPersons.add(new Person("User 1", "user1@gmail.com"));
        mPersons.add(new Person("User 2", "user2@gmail.com"));
        mPersons.add(new Person("User 3", "user3@gmail.com"));

        mPersonsRecyclerViewAdapter = new PersonsRecyclerViewAdapter(mPersons);

        mPersonsRecyclerView = (RecyclerView) findViewById(R.id.personsRecyclerView);
        mPersonsRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mPersonsRecyclerView.setAdapter(mPersonsRecyclerViewAdapter);

        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        mNameEditText = (EditText) findViewById(R.id.nameEditText);
        mEmailEditText = (EditText) findViewById(R.id.emailText);
        mEmailEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    done();
                    return false;
                }
                return true;
            }
        });
        findViewById(R.id.addButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                done();
            }
        });
    }

    private void done() {
        boolean dataValid = true;
        TextInputLayout nameInputLayout = (TextInputLayout) findViewById(R.id.name_input_layout);
        if (TextUtils.isEmpty(mNameEditText.getText())) {
            nameInputLayout.setErrorEnabled(true);
            nameInputLayout.setError("You need to enter a name");
            dataValid = false;
        } else {
            nameInputLayout.setErrorEnabled(false);
            nameInputLayout.setError(null);
        }

        TextView emailError = (TextView) findViewById(R.id.emailError);

        if (!android.util.Patterns.EMAIL_ADDRESS.matcher(mEmailEditText.getText()).matches()) {
            emailError.setVisibility(View.VISIBLE);
            emailError.announceForAccessibility(emailError.getText());
            dataValid = false;
        } else {
            emailError.setVisibility(View.GONE);
        }

        if (dataValid) {
            if (getCurrentFocus() != null) {
                hideKeyboard(getCurrentFocus());
            }
            mPersons.add(new Person(mNameEditText.getText().toString(), mEmailEditText.getText().toString()));
            mPersonsRecyclerViewAdapter.notifyItemInserted(mPersons.size() - 1);
            mNameEditText.setText("");
            mEmailEditText.setText("");

            Runnable runnable = new Runnable() {
                @Override
                public void run() {
                    Snackbar.make(mCoordinatorLayout, "Added", Snackbar.LENGTH_LONG).show();
                }
            };

            Handler handler = new Handler();
            handler.postDelayed(runnable, 500);
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

}