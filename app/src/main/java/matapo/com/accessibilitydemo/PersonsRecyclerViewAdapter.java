package matapo.com.accessibilitydemo;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class PersonsRecyclerViewAdapter extends RecyclerView.Adapter<PersonsRecyclerViewAdapter.PersonViewHolder> {

    private List<Person> mPersons;

    public PersonsRecyclerViewAdapter(List<Person> persons) {
        mPersons = persons;
    }

    @Override
    public int getItemCount() {
        return mPersons.size();
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_person, viewGroup, false);
        return new PersonViewHolder(view);
    }

    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {
        personViewHolder.mPersonName.setText(mPersons.get(i).mName);
        personViewHolder.personAge.setText(mPersons.get(i).mEmail);
    }

    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        TextView mPersonName;
        TextView personAge;

        PersonViewHolder(View itemView) {
            super(itemView);
            mPersonName = (TextView) itemView.findViewById(R.id.personName);
            personAge = (TextView) itemView.findViewById(R.id.personEmail);
        }
    }

}