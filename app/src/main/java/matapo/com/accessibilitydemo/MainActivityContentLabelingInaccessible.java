package matapo.com.accessibilitydemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class MainActivityContentLabelingInaccessible extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_content_labeling_inaccessible);
    }

}
